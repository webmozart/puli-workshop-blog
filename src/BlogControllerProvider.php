<?php

namespace Acme\Blog;

use Silex\Application;
use Silex\ControllerProviderInterface;

class BlogControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'Acme\\Blog\\Controller\\BlogController::index');

        return $controllers;
    }
}
