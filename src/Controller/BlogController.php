<?php

namespace Acme\Blog\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class BlogController
{
    public function index(Application $app)
    {
        return new Response('Hello Blog!');
    }
}
